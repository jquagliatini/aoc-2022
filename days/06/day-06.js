const fs = require('node:fs/promises')

async function main() {
	const line = await fs.readFile('./input.txt', 'utf-8')

	console.log(`day 6 part 1 => ${countStartingBlock(line, 4)}`)
	console.log(`day 6 part 2 => ${countStartingBlock(line, 14)}`)
}

function countStartingBlock(line, blockSize = 4) {
	const chars = line.split('')
	for (let i = 0; i < chars.length - blockSize; i++) {
		if (new Set(chars.slice(i, i + blockSize)).size === blockSize) return i + blockSize
	}

	return -1
}


if (require.main === module) {
	main().catch(console.error)
}
