package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDaySixPartOne(t *testing.T) {
	assert.Equal(t, 7, DaySixPartOne("mjqjpqmgbljsphdztnvjfqwrcgsmlb"))
	assert.Equal(t, 5, DaySixPartOne("bvwbjplbgvbhsrlpgdmjqwftvncz"))
	assert.Equal(t, 6, DaySixPartOne("nppdvjthqldpwncqszvftbrmjlhg"))
	assert.Equal(t, 10, DaySixPartOne("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"))
	assert.Equal(t, 11, DaySixPartOne("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))
}

func TestDaySixPartTwo(t *testing.T) {
	assert.Equal(t, 19, DaySixPartTwo("mjqjpqmgbljsphdztnvjfqwrcgsmlb"))
	assert.Equal(t, 23, DaySixPartTwo("bvwbjplbgvbhsrlpgdmjqwftvncz"))
	assert.Equal(t, 23, DaySixPartTwo("nppdvjthqldpwncqszvftbrmjlhg"))
	assert.Equal(t, 29, DaySixPartTwo("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"))
	assert.Equal(t, 26, DaySixPartTwo("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"))
}
