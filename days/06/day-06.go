package main

import (
	"fmt"

	"telary.io/jordan/aoc/2022/utils"
)

func main() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	daySixPartOne := DaySixPartOne(lines[0])
	fmt.Println("Day 6 - Part 1 =>", daySixPartOne)

	daySixPartTwo := DaySixPartTwo(lines[0])
	fmt.Println("Day 6 - Part 2 =>", daySixPartTwo)
}

func DaySixPartOne(line string) int {
	lineLength := len(line)

	for i := 0; i < lineLength-4; i++ {
		if allDifferent(line[i : i+4]) {
			return i + 4
		}
	}

	return -1
}

func DaySixPartTwo(line string) int {
	lineLength := len(line)

	for i := 0; i < lineLength-14; i++ {
		if allDifferent(line[i : i+14]) {
			return i + 14
		}
	}

	return -1
}

func allDifferent(chars string) bool {
	charsLen := len(chars)
	var uniqCount = make(map[rune]bool, charsLen)
	for _, char := range chars {
		uniqCount[char] = true
	}

	return len(uniqCount) == charsLen
}
