const fs = require('node:fs/promises')

async function main() {
	const lines = (await fs.readFile('./input.txt', 'utf-8')).split('\n')
	const idx = lines.findIndex(x => /^\s*$/.test(x))

	const stacks = parseStacks(lines.slice(0, idx - 1))
	const instructions = lines.slice(idx + 1).filter(x => !/^\s*$/.test(x))

	const partOneStacks = deepClone(stacks)
	const partTwoStacks = deepClone(stacks)

	console.log(`Day 5 Part 1 => ${evalInstructions(partOneStacks, instructions, evalInstructionPartOne)}`)
	printStacks(partOneStacks)

	console.log(`Day 5 Part 2 => ${evalInstructions(partTwoStacks, instructions, evalInstructionPartTwo)}`)
	printStacks(partTwoStacks)
}

function parseStacks(stackLines) {
	const lines = stackLines
		.map(line => line
			.replace(/ {4}/g, ' ')
			.replace(/\[(\w)\]\s?/g, '$1')
		)
	const toNormalizeSize = Math.max(...lines.map(l => l.length))
	const normalizedLines = lines
		.map(l => l.length < toNormalizeSize
				? l + ' '.repeat(toNormalizeSize - l.length)
				: l
		)
	let stacks = Array.from({ length: toNormalizeSize })
		.map((_, i) => normalizedLines.map(n => n[i]).filter(x => x !== ' '))

	return stacks
}

function evalInstructions(stacks, instructions, evalInstruction) {
	for (const instruction of instructions) {
		evalInstruction(stacks, instruction)
	}

	return(
		stacks.map(s => s[0]).join('')
	)
}

function evalInstructionPartOne(stacks, instruction) {
	const i = Instruction(instruction)
	stacks[i.targetStack].unshift(
		...stacks[i.baseStack].splice(0, i.containerCount).reverse()
	)
}

function evalInstructionPartTwo(stacks, instruction) {
	const i = Instruction(instruction)
	stacks[i.targetStack].unshift(
		...stacks[i.baseStack].splice(0, i.containerCount)
	)
}

function Instruction(instructionString) {
	const re = /move (?<container_count>\d+) from (?<base_stack>\d+) to (?<target_stack>\d+)/
	const matched = re.exec(instructionString)
	if (!matched) {
		throw new Error(`Invalid instruction: "${instruction}"`)
	}

	return { targetStack: matched.groups.target_stack - 1, baseStack: matched.groups.base_stack - 1, containerCount: matched.groups.container_count }
}

function printStacks(stacks) {
	const max = Math.max(...stacks.map(s => s.length))
	console.log('\n' +
		Array.from({ length: max })
			.map((_, i) => i)
			.reverse()
			.map(i => stacks.map(s => s[i] ? `[${s[i]}]` : ' '.repeat(3)).join(' '))
			.join('\n') + '\n' + stacks.map((_, i) => ` ${i + 1} `).join(' ') + '\n'
	)
}

function deepClone(x) {
	return JSON.parse(JSON.stringify(x))
}


if (require.main === module) {
	main().catch(console.error)
}
