package main

import (
	"errors"
	"fmt"
	"sort"
	"strconv"

	"telary.io/jordan/aoc/2022/utils"
)

func main() {
	DayOne()
	DayOnePartTwo()
}

func DayOne() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	max, err := FindMaxCalories(lines)
	if err != nil {
		panic(err)
	}

	fmt.Println("Found max calories", max)
}

func DayOnePartTwo() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	topThree, err := FindTopThree(lines)
	if err != nil {
		panic(err)
	}

	fmt.Println("Found top three: ", topThree)
}

func FindTopThree(lines []string) (int, error) {
	calories, err := CountCalories(lines)
	if err != nil {
		return -1, err
	}

	sort.Sort(sort.Reverse(sort.IntSlice(calories)))

	sum := 0
	for _, calorie := range calories[:3] {
		sum = sum + calorie
	}
	return sum, nil
}

func FindMaxCalories(lines []string) (int, error) {
	calories, err := CountCalories(lines)
	if err != nil {
		return -1, err
	}

	max, err := Max(calories)
	if err != nil {
		return -1, err
	}

	return max, nil
}

func CountCalories(lines []string) ([]int, error) {
	var sums []int
	current_sum := 0

	for _, line := range lines {
		if line == "" {
			sums = append(sums, current_sum)
			current_sum = 0
			continue
		}

		number, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}

		current_sum += number
	}
	sums = append(sums, current_sum)
	return sums, nil
}

func Max(numbers []int) (int, error) {
	numbersCopy := make([]int, len(numbers))
	copy(numbersCopy, numbers)

	sort.Sort(sort.Reverse(sort.IntSlice(numbersCopy)))
	if len(numbersCopy) > 0 {
		return numbersCopy[0], nil
	}

	return -1, errors.New("Empty array")
}
