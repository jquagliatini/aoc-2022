package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var INPUT = []string{"1000", "2000", "3000", "", "4000", "", "5000", "6000", "", "7000", "8000", "9000", "", "10000"}

func TestCountCalories(t *testing.T) {
	calories, err := CountCalories(INPUT)

	assert.Nil(t, err)
	assert.Equal(t, []int{6000, 4000, 11000, 24000, 10000}, calories)
}

func TestMax(t *testing.T) {
	max, err := Max([]int{23, 1, 15, 32, 41, 12})

	assert.Nil(t, err)
	assert.Equal(t, 41, max)
}

func TestFindMaxCalories(t *testing.T) {
	max, err := FindMaxCalories(INPUT)

	assert.Nil(t, err)
	assert.Equal(t, 24000, max)
}

func TestFindTopThree(t *testing.T) {
	topThree, err := FindTopThree(INPUT)

	assert.Nil(t, err)
	assert.Equal(t, 45000, topThree)
}
