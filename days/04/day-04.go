package main

import (
	"fmt"
	"strconv"
	"strings"

	"telary.io/jordan/aoc/2022/utils"
)

func main() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	lineCount := DayFourPartOne(lines)
	fmt.Println("Day 4 - Part 1:", lineCount)

	lineCount = DayFourPartTwo(lines)
	fmt.Println("Day 4 - Part 2:", lineCount)
}

func DayFourPartOne(lines []string) int {
	sum := 0
	for _, line := range lines {
		slices := strings.Split(line, ",")
		if SlicesAreFullyContainedWithinTheOther(slices[0], slices[1]) {
			sum++
		}
	}
	return sum
}

func DayFourPartTwo(lines []string) int {
	sum := 0
	for _, line := range lines {
		sections := strings.Split(line, ",")
		if SectionsOverlap(sections[0], sections[1]) {
			sum++
		}
	}
	return sum
}

func SlicesAreFullyContainedWithinTheOther(first string, second string) bool {
	firstAssignment := ParseRange(first)
	secondAssignment := ParseRange(second)

	return (firstAssignment.start <= secondAssignment.start &&
		firstAssignment.end >= secondAssignment.end) ||
		(secondAssignment.start <= firstAssignment.start &&
			secondAssignment.end >= firstAssignment.end)
}

func ParseRange(numberRange string) Assignment {
	var numbers []int
	numberStrings := strings.Split(numberRange, "-")
	for _, number := range numberStrings {
		n, err := strconv.Atoi(number)
		if err != nil {
			panic(err)
		}
		numbers = append(numbers, n)
	}

	return Assignment{start: numbers[0], end: numbers[1]}
}

type Assignment struct {
	start int
	end   int
}

func SectionsOverlap(firstSection string, secondSection string) bool {
	firstAssignment := ParseRange(firstSection)
	secondAssignment := ParseRange(secondSection)

	return SlicesAreFullyContainedWithinTheOther(firstSection, secondSection) ||
		(secondAssignment.start <= firstAssignment.end &&
			secondAssignment.start >= firstAssignment.start) ||
		(secondAssignment.end >= firstAssignment.start &&
			secondAssignment.end <= firstAssignment.end)
}
