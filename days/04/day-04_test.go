package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseRange(t *testing.T) {
	assert.Equal(t, Assignment{start: 1, end: 4}, ParseRange("1-4"))
}

func TestSlicesAreFullyContainedWithinTheOther(t *testing.T) {
	assert.True(t, SlicesAreFullyContainedWithinTheOther("1-4", "4-4"))
	assert.True(t, SlicesAreFullyContainedWithinTheOther("1-4", "2-4"))
}

func TestSlicesAreFullyContainedWithinTheOtherShouldFail(t *testing.T) {
	assert.False(t, SlicesAreFullyContainedWithinTheOther("1-4", "5-6"))
}

func TestDayFourPartOne(t *testing.T) {
	input := []string{
		"2-4,6-8",
		"2-3,4-5",
		"5-7,7-9",
		"2-8,3-7",
		"6-6,4-6",
		"2-6,4-8",
	}
	assert.Equal(t, 2, DayFourPartOne(input))
}

func TestSectionsOverlap(t *testing.T) {
	assert.True(t, SectionsOverlap("5-7", "7-9"))
	assert.True(t, SectionsOverlap("2-8", "3-7"))
	assert.True(t, SectionsOverlap("6-6", "4-6"))
	assert.True(t, SectionsOverlap("2-6", "4-8"))
	assert.True(t, SectionsOverlap("22-79", "21-79"))
	assert.True(t, SectionsOverlap("22-79", "31-85"))
	assert.True(t, SectionsOverlap("33-34", "17-35"))
}

func TestSectionsDontOverlap(t *testing.T) {
	assert.False(t, SectionsOverlap("2-3", "4-5"))
	assert.False(t, SectionsOverlap("21-22", "26-28"))
}

func TestDayFourPartTwo(t *testing.T) {
	input := []string{
		"2-4,6-8",
		"2-3,4-5",
		"5-7,7-9",
		"2-8,3-7",
		"6-6,4-6",
		"2-6,4-8",
	}
	assert.Equal(t, 4, DayFourPartTwo(input))
}
