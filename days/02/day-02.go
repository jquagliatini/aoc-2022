package main

import (
	"errors"
	"fmt"
	"strings"

	"telary.io/jordan/aoc/2022/utils"
)

func main() {
	DayTwoPartOne()
	DayTwoPartTwo()
}

func DayTwoPartOne() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	score := ParseStrategyPartOne(lines)
	fmt.Println("Score: ", score)
}

func DayTwoPartTwo() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	score := ParseStrategyPartTwo(lines)
	fmt.Println("Part 2 - Score: ", score)
}

func ParseStrategyPartOne(lines []string) int {
	sum := 0
	for _, line := range lines {
		score := parseStrategyPartOneLine(line)
		sum = sum + score
	}

	return sum
}

func parseStrategyPartOneLine(line string) int {
	draw := strings.Split(line, " ")
	strategy, err := toStrategyLine(draw[1])

	if err != nil {
		panic(err)
	}

	if strategy.defeats == draw[0] {
		return 6 + strategy.points
	}

	if strategy.opponentName == draw[0] {
		return 3 + strategy.points
	}

	return 0 + strategy.points
}

func ParseStrategyPartTwo(lines []string) int {
	score := 0
	for _, line := range lines {
		drawScore := parseStrategyPartTwoLine(line)
		score = score + drawScore
	}
	return score
}

func parseStrategyPartTwoLine(line string) int {
	draw := strings.Split(line, " ")

	strategy, err := toStrategyLine(draw[0])
	if err != nil {
		panic(err)
	}

	switch draw[1] {
	case "X":
		s, err := toStrategyLine(strategy.defeats)
		if err != nil {
			panic(err)
		}
		return s.points
	case "Y":
		return strategy.points + 3
	case "Z":
		switch draw[0] {
		case Rock.defeats:
			return Rock.points + 6
		case Paper.defeats:
			return Paper.points + 6
		case Scissors.defeats:
			return Scissors.points + 6
		}
	}

	return 0
}

type StrategyLine struct {
	opponentName string
	name         string
	defeats      string
	points       int
}

func toStrategyLine(c string) (StrategyLine, error) {
	switch c {
	case "A", "X":
		return Rock, nil
	case "B", "Y":
		return Paper, nil
	case "C", "Z":
		return Scissors, nil
	}

	return StrategyLine{}, errors.New("Unknown character " + c)
}

var Rock = StrategyLine{
	name:         "X",
	opponentName: "A",
	defeats:      "C",
	points:       1,
}

var Paper = StrategyLine{
	name:         "Y",
	opponentName: "B",
	defeats:      "A",
	points:       2,
}

var Scissors = StrategyLine{
	name:         "Z",
	opponentName: "C",
	defeats:      "B",
	points:       3,
}
