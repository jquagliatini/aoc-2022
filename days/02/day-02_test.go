package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var INPUT = []string{"A Y", "B X", "C Z"}

func TestParse(t *testing.T) {
	score := ParseStrategyPartOne(INPUT)
	assert.Equal(t, 15, score)
}

func TestParsePartTwo(t *testing.T) {
	score := ParseStrategyPartTwo(INPUT)
	assert.Equal(t, 12, score)
}
