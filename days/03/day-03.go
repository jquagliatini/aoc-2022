package main

import (
	"fmt"
	"math"

	"telary.io/jordan/aoc/2022/utils"
)

func main() {
	lines, err := utils.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	day3Part1 := DayThreePartOne(lines)
	fmt.Printf("Day 3 - Part1 => %d\n", day3Part1)

	day3Part2 := DayThreePartTwo(lines)
	fmt.Printf("Day 3 - Part2 => %d\n", day3Part2)
}

func DayThreePartOne(lines []string) int {
	sum := 0
	for _, line := range lines {
		commonLetter := FindCommonLetterBetweenRucksacks(line)
		priority := Priority(commonLetter)
		sum = sum + priority
	}
	return sum
}

func DayThreePartTwo(lines []string) int {
	sum := 0
	for _, lines3 := range utils.Window(lines, 3) {
		commonLetter := FindCommonLetterBetweenMultipleRucksacks(lines3)
		sum = sum + Priority(commonLetter)
	}
	return sum
}

func FindCommonLetterBetweenRucksacks(rucksack string) rune {
	size := float64(len(rucksack))
	half := int64(math.Round(size / 2))

	firstHalf := uniq(rucksack[:half])
	secondHalf := uniq(rucksack[half:])

	for _, c := range firstHalf {
		if exists(secondHalf, c) {
			return c
		}
	}

	return ' '
}

func FindCommonLetterBetweenMultipleRucksacks(rucksacks []string) rune {
	if len(rucksacks) != 3 {
		panic("Can't find common letter for less than 3 rucksacks")
	}

	for _, c := range rucksacks[0] {
		if exists(rucksacks[1], c) && exists(rucksacks[2], c) {
			return c
		}
	}
	return ' '
}

func exists(s string, c rune) bool {
	for _, cc := range s {
		if cc == c {
			return true
		}
	}

	return false
}

func uniq(str string) string {
	foundValues := map[rune]bool{}
	var result string
	for _, c := range str {
		if foundValues[c] != true {
			foundValues[c] = true
			result = result + string(c)
		}
	}

	return result
}

func Priority(letter rune) int {
	if letter == ' ' {
		return 0
	}

	c := int(letter)

	switch {
	case c > 96:
		return c - 96
	case c > 64:
		return (c - 64) + 26
	}

	return 0
}
