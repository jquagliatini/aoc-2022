package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var INPUT = []string{
	"vJrwpWtwJgWrhcsFMMfFFhFp",
	"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
	"PmmdzqPrVvPwwTWBwg",
	"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
	"ttgJtRGJQctTZtZT",
	"CrZsJsPPZsGzwwsLwLmpwMDw",
}

func TestFindCommonLetterBetweenRucksacks(t *testing.T) {
	assert.Equal(
		t,
		'p',
		FindCommonLetterBetweenRucksacks(INPUT[0]),
	)
	assert.Equal(
		t,
		'L',
		FindCommonLetterBetweenRucksacks(INPUT[1]),
	)
}

func TestPriority(t *testing.T) {
	assert.Equal(t, 16, Priority('p'))
	assert.Equal(t, 38, Priority('L'))
	assert.Equal(t, 42, Priority('P'))
	assert.Equal(t, 22, Priority('v'))
	assert.Equal(t, 20, Priority('t'))
	assert.Equal(t, 19, Priority('s'))
	assert.Equal(t, 18, Priority('r'))
	assert.Equal(t, 52, Priority('Z'))
}

func TestDayThreePartOne(t *testing.T) {
	assert.Equal(t, 157, DayThreePartOne(INPUT))
}

func TestDayThreePartTwo(t *testing.T) {
	assert.Equal(t, 70, DayThreePartTwo(INPUT))
}

func TestFindCommonLetterBetweenMultipleRucksacks(t *testing.T) {
	letter := FindCommonLetterBetweenMultipleRucksacks(INPUT[:3])
	assert.Equal(t, 'r', letter)
	letter = FindCommonLetterBetweenMultipleRucksacks(INPUT[3:])
	assert.Equal(t, 'Z', letter)
}
