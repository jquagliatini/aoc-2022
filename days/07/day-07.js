const fs = require("node:fs/promises");

async function main() {
  const lines = (await fs.readFile("./input.txt", "utf-8")).split('\n');
	const tree = parseLines(lines)

  const partOneSum = partOne(tree)
  console.log('Day 7 - Part 1 =>', partOneSum)

	const partTwoSize = partTwo(tree)
	console.log('Day 7 - Part 2 =>', partTwoSize)
}

function partOne(tree) {
  return findAllDirectoriesAtMost100000(
    tree
  ).reduce((sum, node) => sum + node.deepSize, 0)
}

function partTwo(tree) {
	return findTinyestTree(
		findDeletableDirectories(
			tree
		)
	)?.deepSize
}

class Tree {
  /**
   * @param {{ name: string size?: number }} node
   * @param {(Tree | { name: string size?: number })[]} children
   */
  constructor(node, ...children) {
    this.name = node.name;
    this.size = node.size ?? 0;
    this.children = [];
    this.addChildren(...children);
  }

  get(name) {
    return this.children.find((child) => child.name === name);
  }

  [Symbol.iterator]() {
    return [{ size: this.size, name: this.name }]
      .concat(this.children.flatMap((child) => [...child]))
      [Symbol.iterator]();
  }

  addChildren(...children) {
    this.children.push(
      ...children
        .map((child) =>
          child instanceof Tree
            ? child
            : typeof child === "object" && "name" in child
            ? new Tree({ name: child.name, size: child.size })
            : undefined
        )
        .filter(Boolean)
    );
    return this;
  }

  static fromRoot(...items) {
    return new Tree({ name: "/", size: 0 }, ...items);
  }

  get deepSize() {
    return [...this].reduce((sum, x) => sum + x.size, 0);
  }
}

/**
 * @param {string[]} lines
 * @param {Tree} tree
 * @returns {Tree}
 */
function parseLines(lines, tree) {
  while (lines.length) {
    const line = lines.shift();
    if (line.startsWith("$ ls") || /^\s*$/.test(line)) {
      continue;
    } else if (line.startsWith("$ cd")) {
      const nodeName = line.replace("$ cd ", "");
      if (nodeName === "..") {
        return tree;
      }

      lines.shift();
      parseLines(
        lines,
        tree?.get(nodeName) ??
          (
            tree?.addChildren({ name: nodeName }) ??
            (tree = new Tree({ name: nodeName }))
          ).get(nodeName) ??
          tree
      );
    } else if (line.startsWith("dir")) {
      const dirName = line.replace("dir ", "");
      tree.addChildren(new Tree({ name: dirName }));
    } else {
      const [fileSize, fileName] = line.split(" ");
      tree.addChildren(
        new Tree({
          name: fileName,
          size: Number(fileSize),
        })
      );
    }
  }

  return tree;
}



/** @param {Tree} tree */
function findAllDirectoriesAtMost100000(tree) {
  return (tree.size === 0 && tree.deepSize <= 100_000 ? [tree] : [])
    .concat(tree.children.flatMap(findAllDirectoriesAtMost100000))
}

/**
 * @param {Tree} tree
 * @returns {Tree[]}
*/
function findDeletableDirectories(tree, freeSpace = -1) {
	if (freeSpace === -1) {
		freeSpace = 70000000 - tree.deepSize
	}

	return tree.size === 0 && freeSpace + tree.deepSize > 30000000
		? [tree]
				.concat(
					tree.children.flatMap(child => findDeletableDirectories(child, freeSpace))
				)
		: []
}

/**
 * @param {[Tree, ...Tree[]]} trees
 * @returns {Tree | undefined}
 */
function findTinyestTree(trees) {
	let minTree = new Tree({ name: '__', size: Number.MAX_VALUE });
	for (const tree of trees) {
		if (tree.deepSize < minTree.deepSize) {
			minTree = tree
		}
	}

	return minTree.name === '__' ? undefined : minTree
}

module.exports = { Tree, parseLines, findAllDirectoriesAtMost100000, findDeletableDirectories };

if (module === require.main) {
  main().catch(console.error);
}
