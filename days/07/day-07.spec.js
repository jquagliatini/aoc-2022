const assert = require("assert/strict");
const {
  Tree,
  parseLines,
  findAllDirectoriesAtMost100000,
	findDeletableDirectories
} = require("./day-07");

const tree = Tree.fromRoot(
  new Tree(
    { name: "a" },
    { name: "b.txt", size: 10_000 },
    new Tree({ name: "c" }, { name: "cc.dat", size: 1_000 })
  )
);

assert.deepEqual(
  [...tree],
  [
    { name: "/", size: 0 },
    { name: "a", size: 0 },
    { name: "b.txt", size: 10_000 },
    { name: "c", size: 0 },
    { name: "cc.dat", size: 1_000 },
  ]
);

assert.equal(tree.deepSize, 11_000);

const EXPECTED_TREE = Tree.fromRoot(
  new Tree(
    { name: "a" },
    new Tree({ name: "e" }, { name: "i", size: 584 }),
    { name: "f", size: 29116 },
    { name: "g", size: 2557 },
    { name: "h.lst", size: 62596 }
  ),
  { name: "b.txt", size: 14848514 },
  { name: "c.dat", size: 8504156 },
  new Tree(
    { name: "d" },
    { size: 4060174, name: "j" },
    { size: 8033020, name: "d.log" },
    { size: 5626152, name: "d.ext" },
    { size: 7214296, name: "k" }
  )
);

assert.deepEqual(
  parseLines(
    `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
`.split("\n")
  ),
  EXPECTED_TREE
);

assert.deepEqual(findAllDirectoriesAtMost100000(EXPECTED_TREE), [
  new Tree(
    { name: "a" },
    new Tree({ name: "e" }, { name: "i", size: 584 }),
    { name: "f", size: 29116 },
    { name: "g", size: 2557 },
    { name: "h.lst", size: 62596 }
  ),
  new Tree({ name: "e" }, { name: "i", size: 584 }),
]);

assert.deepEqual(
	findDeletableDirectories(EXPECTED_TREE),
	[
		EXPECTED_TREE,
		new Tree(
			{ name: "d" },
			{ size: 4060174, name: "j" },
			{ size: 8033020, name: "d.log" },
			{ size: 5626152, name: "d.ext" },
			{ size: 7214296, name: "k" }
		)
	]
)
