package utils

func Window[Type any](slice []Type, size int) [][]Type {
	var outSlice [][]Type

	tmpArray := make([]Type, size)

	for i := range slice {
		if i > 0 && i%size == 0 {
			dst := make([]Type, size)
			copy(dst, tmpArray)
			outSlice = append(outSlice, dst)

			tmpArray = make([]Type, size)
		}

		tmpArray[i%size] = slice[i]
	}

	outSlice = append(outSlice, tmpArray)

	return outSlice
}
